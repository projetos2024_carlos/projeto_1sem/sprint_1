module.exports = class userController {

  static async postUser(req, res) {
    const { name, apartment, telephone, email, password } = req.body;

    if (!/^[A-Z][a-zA-Z]+$/.test(name)) {
      return res.status(400).json({ message: 'O nome deve iniciar com uma letra maiúscula.' });
    }

    if (!/@/.test(email)) {
      return res.status(400).json({ message: 'O email deve conter um símbolo @.' });
    }

    if (password.length < 8) {
      return res.status(400).json({ message: 'A senha deve ter no mínimo 8 caracteres.' });
    }

    if (!name || !apartment || !telephone || !email || !password) {
      return res.status(400).json({ message: 'Preencha todos os campos.' });
    } else {
      return res.status(200).json({ message: 'Usuário cadastrado!' });
    }
  }

  static async putUser(req, res) { //Talvez adicionar futuramente uma requisição por ID para atualizações no usuário
    const { name, apartment, telephone, email, password } = req.body;

    if (name &&!/^[A-Z][a-zA-Z]+$/.test(name)) {
      return res.status(400).json({ message: 'O nome deve iniciar com uma letra maiúscula.' });
    }

    if (email &&!/@/.test(email)) {
      return res.status(400).json({ message: 'O email deve conter um símbolo @.' });
    }

    if (password && password.length < 8) {
      return res.status(400).json({ message: 'A senha deve ter no mínimo 8 caracteres.' });
    }

    if (!name ||!apartment ||!telephone ||!email ||!password) {
      return res.status(400).json({ message: "Usuário com informações faltantes para a edição!" });
    } else {
      return res.status(201).json({ message: "Usuário Editado!" });
    }

  }

  static async deleteUser(req, res) { //Talvez trocar os ID's sendo passados nas rotas para algo interno para mais privacidade para os dados do user
    if (req.params.id !== "") {
      res.status(200).json({ message: " O usuário foi deletado. " });
    } else {
      res.status(400).json({ message: " Usuário não encontrado. " });
    }
  }

  static async getUsers(req, res) { //Talvez trocar os ID's sendo passados nas rotas para algo interno para mais privacidade para os dados do user
    if (req.params.id !== "") {
      res.status(200).json({ message: " O usuário foi encontrado. "});
    } else {
      res.status(400).json({ message: "O usuário não foi encontrado"});
    }
  }

  static async getLogin(req, res) {
      if (req.params.id !== "") { //Quando realizadas as conexões com o BD, realizar as validações extraindo o ID do usuario do BD
        res.status(200).json({ message: `Bem-vindo(a)!` }); //Quando realizadas as conexões com o banco de dados, alterar o codigo para que o usuário receba suas informações no getLogin como nome e email
      } else {
        res.status(400).json({ message: "Por Favor, insira um ID." });
      }
    }

  static async postLogin(req, res) {
    const { email, password } = req.body;

    if (!email ||!password) {
      return res.status(400).json({ message: 'Por Favor, insira e-mail e senha.' });
    }

    if (!/@/.test(email)) {
      return res.status(400).json({ message: 'Insira um e-mail válido.' });
    }

    if (password.length < 8) {
      return res.status(400).json({ message: 'A senha deve conter no mínimo 8 caracteres.' });
    }

    if (res.status !== (400) ) {
      return res.status(200).json({ message: "Login bem sucedido." });
    }
  }

}
//Trocar as mensagens em json para alertas futuramente