const express = require('express');

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes');
      this.express.use('/sistema', apiRoutes);
    }
}

module.exports = new AppController().express;
