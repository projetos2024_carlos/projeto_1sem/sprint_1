const router = require('express').Router()
const userController = require('../controller/userController')

router.post('/cadastro/', userController.postUser);
router.get('/getuser/:id', userController.getUsers);
router.put('/updateuser/', userController.putUser);
router.delete('/deleteuser/:id', userController.deleteUser);

router.post('/login', userController.postLogin);
router.get('/login/:id', userController.getLogin);


module.exports = router